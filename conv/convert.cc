
#include <iostream>
#include <fstream>
#include <string>

#include "TFile.h"
#include "TTree.h"

int main( int argc, char** argv )
{
   /// read arg 
   if( argc!=3 ){
      std::cout<<" arg err: (bin) (input) (output)" << std::endl;
      return 0; 
   }

   const std::string ifname = argv[1];
   const std::string ofname = argv[2];
   std::cout<<" input file  : " << ifname << std::endl;
   std::cout<<" output file : " << ofname << std::endl;

   // prepare input 
   std::ifstream ifile( ifname.c_str() );
   if( !ifile.is_open() ){
      std::cout<<" Fail to open input file. " << std::endl;
      return 0;
   }
   
   // prepare output
   TFile *ofile = new TFile( ofname.c_str(), "recreate");
   TTree *otree = new TTree( "wfmTree", "converted from ADC output.");

   UShort_t Data[16][64];
   otree->Branch("Data", Data, "Data[16][64]/s");

   int dummy = 0;
   // start conversion
   // skip first 6 zeros 
   for( int i=0; i<6; i++ )
   { 
      ifile >> std::hex >> dummy;
      //std::cout<<" dummy = " << std::hex << dummy << std::endl;
   }
   

   unsigned int iword = 0;
   int r_data;
   int event_id = 0;
   while( ifile >> std::hex >> r_data )
   {
      if( iword==0 ) std::cout<<" Process for event" << event_id << std::endl; 

      //std::cout<< std::dec << iword << "\t" << std::hex << r_data << std::endl;

      // 6 header word ///
      if( iword<6 ){
         if( (r_data&0xc000)!=0xc000 ){
            std::cout<<" header error! " << std::endl;
            std::cout<< iword << "\t" << std::hex << (r_data&0xc000) << std::endl;
            return 0;
         }         
      /// energy word ///
      }else if( iword <6 + 1024 ){
         unsigned int ch_id = (iword-6) % 16;
         unsigned int isam  = (iword-6) / 16;
         Data[ch_id][isam] = (r_data & 0x3fff);
      }

      /// 6 footer words ///
      if( iword < 6 + 1024 + 6 - 1 ){
         iword++;
      }else{
         iword = 0;
         otree->Fill(); 
         event_id++;
     }
   } 

   // end
   std::cout<<" END " << std::endl;
   otree->Write();
   ofile->Close();

   return 1;
}
